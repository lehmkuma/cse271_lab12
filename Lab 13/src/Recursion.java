
public class Recursion {

    /**
     * uses recursion to get the
     *  total of an exponent.
     * 
     * @param base the number 
     *  being multiplied by the exponent.
     * @param n the exponent
     * @return this is what recurse the method. 
     */
    public static int powerN(int base, int n) {
        
        if (n == 0) {
            return 1;
        }
           
        return (base * powerN(base, n - 1));
    }
    
    /**
     * gets the total number of block in the triangle.
     * 
     * @param row number of blocks in the row.
     * @return this is what recurse the method.
     */
    public static int triangle(int row) {
        
        if (row == 0) {
            return 0;
        }
        
        return row + triangle(row - 1);
    }
}
